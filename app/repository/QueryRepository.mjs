import {ShippingContainers, YgBox} from '../model/index.mjs'

// export default 
class QueryRepository {

    findShippingContainer(id){
        return new Promise((resolve, reject) => {
            ShippingContainers.where('id', id)
                .with('shippings', (q)=>{
                    q.orderBy('id', 'DESC')
                })
                .with('shippings.shipping_product')
                .with('shippings.packs.products.products')
                .with('shippings.user')
                .with('yg_order', (q) => {
                    q.orderBy('id', 'DESC')
                })
                .with('yg_order.yg_order_detail')
                .with('yg_order.user')
                
                .first()
                .then(result => {
                    if(result) return resolve(result.toJSON())

                    resolve(null)
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    findYgBoxById(id){
        return new Promise((resolve, reject) => {
            YgBox.with('orders.shipping.packs.products.products')
            .with('orders.yg_order.yg_order_detail.yg_product_detail')
            .with('orders.yg_order.user')
            .with('orders.yg_order.yg_packing_receipts')
            .with('orders.shipping.user')
            .with('orders.shipping.shipping_product')
            .where('id', id)
            .first()
            .then(result => {
                if(result) return resolve(result.toJSON())
                resolve(null)
            })
            .catch(err => reject(err))
        })
    }

}

export default QueryRepository;