import excel from 'excel4node'
import QueryRepository from './QueryRepository.mjs'
import {resizeImageByParamsUrl} from '../helper/index.mjs'
import AwsService from '../helper/AwsService.mjs'


class ExportTrackingRepository extends QueryRepository{

    constructor(image){
        super()
        this.wb = new excel.Workbook()
        this.ws = this.wb.addWorksheet('Sheet 1')
        this.subtitleStyle = {
            font: {
                name: 'Times New Roman',
                bold: true,
                size: 12
            },
            alignment:{
                horizontal: 'center',
                vertical: 'center'
            }
        }
        this.row = 8
        this.number = 0
        this.image = image
    }

    
    async generateShippingTracking(ContainerID){
        try {
            let shippingContainer = await this.findShippingContainer(ContainerID)
            if(!shippingContainer) false

            // set header documents
            this.ws.cell(1, 2, 1,6, true).string('DRAFT INVOICE').style({
                font: {name: 'Times New Roman', bold:true, size:20}, 
                alignment:{
                    horizontal: 'center',
                    vertical: 'center'
                }
            });
            this.ws.row(1).setHeight(35)

            this.ws.cell(2, 2, 2, 6, true).string('(14-6) No.586 Jiangnan Road Hi-Tech Zone Ningbo China').style(this.subtitleStyle);
            this.ws.cell(3, 1).string('Buyer :').style(this.subtitleStyle);
            this.ws.cell(3, 2, 3, 3, true).string('PT. EKOSISTEM IMPORTIR DIGITAl').style({
                font: {
                    name: 'Times New Roman',
                    bold: true,
                    size: 10
                }
            });

            this.ws.cell(4, 2, 4, 3, true).string('JL. CIPUTAT RAYA NO.5., KEL. PONDOK').style({
                font: {
                    name: 'Times New Roman',
                    bold: true,
                    size: 10
                }
            });

            this.ws.cell(5, 2, 5,3, true).string('PINANG, KEBAYORAN LAMA, KOTA ADM.').style({
                font: {
                    name: 'Times New Roman',
                    bold: true,
                    size: 10
                }
            });

            this.ws.cell(6, 2, 6, 3, true).string('JAKARTA SELATAN, PROP DKI JAKARTA').style({
                font: {
                    name: 'Times New Roman',
                    bold: true,
                    size: 10
                }
            });

            this.subtitleStyle.alignment = {
                horizontal: 'right',
            }
            this.ws.cell(3, 8).string('Invoice No:').style(this.subtitleStyle);

            this.ws.cell(4, 8).string('Date of issue:').style(this.subtitleStyle);
            this.ws.cell(5, 8).string('Shipment Term:').style(this.subtitleStyle);
            this.ws.cell(6, 8).string('Destination Port:').style(this.subtitleStyle);

            const headStyle = this.wb.createStyle(this.headerStyle(true))
            this.ws.row(8).setHeight(35)
            this.ws.cell(8, 1).string('No').style(headStyle)
            this.ws.column(1).setWidth(10);
            this.ws.cell(8, 2).string('Mark Item').style(headStyle)
            this.ws.column(2).setWidth(20);
            this.ws.cell(8, 3).string('Part Name').style(headStyle)
            this.ws.column(3).setWidth(35);
            this.ws.cell(8, 4).string('Part No').style(headStyle)
            this.ws.column(4).setWidth(35);
            this.ws.cell(8, 5).string('Qty').style(headStyle)
            this.ws.column(5).setWidth(10);
            this.ws.cell(8, 6).string('Unit Price').style(headStyle)
            this.ws.column(6).setWidth(25);
            this.ws.cell(8, 7).string('Total Amount').style(headStyle)
            this.ws.column(7).setWidth(25);
            this.ws.cell(8, 8).string('HS Code').style(headStyle)
            this.ws.column(8).setWidth(25);
            this.ws.cell(8, 9).string('Picture').style(headStyle)
            this.ws.column(9).setWidth(16);

            // set invoice no container
            const d = new Date()
            let dateFormat = `${d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}`
            this.ws.cell(3, 9).string(shippingContainer.tracking_number).style(this.subtitleStyle);
            this.ws.cell(4, 9).string(dateFormat).style(this.subtitleStyle);

            const shipping = shippingContainer.shippings
            const order = shippingContainer.yg_order
            // return shipping;

            await this.generateShippingBody(shipping)
            await this.generateOrderBody(order)
            this.footerRow()
            console.log('Generate Finished')

            const fileName = `shipping-traking-${ContainerID}.xlsx`
            // check file if exist on aws s3

            const fileCheck = await AwsService.checkFile(fileName)
            if(fileCheck){
                // delete file if exist
                await AwsService.deleteFile(fileName)
            }
            const stream = await this.wb.writeToBuffer()
            const upload = await AwsService.uploadExcel(stream, fileName)
            return upload
        } catch (error) {
            console.log(error, 'wlsl')
            return error
        }
    }

    headerStyle(bold = false){
        return {
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            font: {
                name: 'Times New Roman',
                bold: bold,
                size: 13
            },
            border: {
                left: {
                    style: 'thin',
                    color: "black" 
                },
                right: {
                    style: 'thin',
                    color: "black"
                },
                top: {
                    style: 'thin',
                    color: "black"
                },
                bottom: {
                    style: 'thin',
                    color: "black"
                }
            }
        }
    }

    fillStyle(){
        return {
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            font: {
                name: 'Times New Roman',
                size: 12
            },
            border: {
                left: {
                    style: 'thin', //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                    color: "black" // HTML style hex value
                },
                right: {
                    style: 'thin',
                    color: "black"
                },
                top: {
                    style: 'thin',
                    color: "black"
                },
                bottom: {
                    style: 'thin',
                    color: "black"
                }
            }
        }
    }

    priceStyle(){
        return {
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            font: {
                name: 'Times New Roman',
                size: 12
            },
            border: {
                left: {
                    style: 'thin', //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                    color: "black" // HTML style hex value
                },
                right: {
                    style: 'thin',
                    color: "black"
                },
                top: {
                    style: 'thin',
                    color: "black"
                },
                bottom: {
                    style: 'thin',
                    color: "black"
                }
            },
            numberFormat: '"RMB "#,##0.00; "RMB "#,##.00; "RMB " 0.00; -'
        }
    }

    generateShippingBody(shipping){
        return new Promise(async(resolve, reject) => {
             // generate body documents
            const bodyStyle = this.wb.createStyle(this.fillStyle())
            const priceFormat = this.wb.createStyle(this.priceStyle())
            let lastShp = ''
            let lastImg = ''
            for (let index = 0; index < shipping.length; index++) {
                let val = shipping[index]
                for (let i = 0; i < val.shipping_product.length; i++) {
                    this.row++
                    this.number++
                    let item = val.shipping_product[i]
                    this.ws.row(this.row).setHeight(90)
                    this.ws.cell(this.row, 1).style(bodyStyle).number(this.number)
                    this.ws.cell(this.row, 2).style(bodyStyle).string(val.invoice_no)
                    this.ws.cell(this.row, 3).style(bodyStyle).string(item.name)
                    this.ws.cell(this.row, 4).style(bodyStyle).string(val.user.marking_code)
                    this.ws.cell(this.row, 5).number(item.quantity).style(bodyStyle)
                    this.ws.cell(this.row, 6).number(item.unit_price_rmb).style(priceFormat)
                    this.ws.cell(this.row, 7).formula(`E${this.row} * F${this.row}`).style(priceFormat)
                    this.ws.cell(this.row, 8).style(bodyStyle).string(item.hs_code)
                    this.ws.cell(this.row, 9).style(bodyStyle)
                    if(this.image){
                            let img = await resizeImageByParamsUrl(process.env.CDN_URL+item.image, true)
                            if(img !== false){
                                await this.ws.addImage({
                                    image: img,
                                    type: 'picture',
                                    width:'15mm',
                                    position: {
                                        type: 'oneCellAnchor',
                                        from: {
                                            col: 9,
                                            colOff: '2mm',
                                            row: this.row,
                                            rowOff: '2mm',
                                        },
                                        x:'10mm',
                                        y:'10mm'
                                    },
                                });
                            }
                    }
                    lastShp = val.invoice_no
                }
            }
            resolve()
        })
    }

    generateOrderBody(order){
        return new Promise(async(resolve, reject) => {
            const bodyStyle = this.wb.createStyle(this.fillStyle())
            const priceFormat = this.wb.createStyle(this.priceStyle())
            for (let index = 0; index < order.length; index++) {
                this.row++
                this.number++
                let item = order[index]
                if(item.yg_order_detail.length){
                    let orderDetail = item.yg_order_detail[0]
                    this.ws.row(this.row).setHeight(90)
                    this.ws.cell(this.row, 1).style(bodyStyle).number(this.number)
                    this.ws.cell(this.row, 2).style(bodyStyle).string(item.invoice_no)
                    this.ws.cell(this.row, 3).style(bodyStyle).string(orderDetail.product_title)
                    this.ws.cell(this.row, 4).style(bodyStyle).string(item.user.marking_code)
                    if(orderDetail.quantity > 0 && orderDetail.price_estimate_per_product > 0){
                        this.ws.cell(this.row, 5).number(orderDetail.quantity).style(bodyStyle)
                        this.ws.cell(this.row, 6).number(parseFloat(orderDetail.price_estimate_per_product)).style(priceFormat)
                        this.ws.cell(this.row, 7).formula(`E${this.row} * F${this.row}`).style(priceFormat)
                    }else{
                        this.ws.cell(this.row, 5).number(parseFloat(orderDetail.quantity)).style(bodyStyle)
                        this.ws.cell(this.row, 6).number(parseFloat(orderDetail.price_estimate_per_product)).style(priceFormat)
                        this.ws.cell(this.row, 7).formula(`E${this.row} * F${this.row}`).style(priceFormat)
                    }
                    this.ws.cell(this.row, 8).style(bodyStyle).string(orderDetail.hscode)
                    this.ws.cell(this.row, 9).style(bodyStyle)
                    if(this.image){
                        let img = false
                        if(item.flag == 'admin_product' || item.flag == 'taobao' || item.flag == 'tmall'){
                            img = await resizeImageByParamsUrl(orderDetail.product_image, true)
                        } else {
                            img = await resizeImageByParamsUrl(orderDetail.product_image, false)
                        }
                        
                        if(img){
                            try {
                                await this.ws.addImage({
                                    image: img,
                                    type: 'picture',
                                    width:'15mm',
                                    position: {
                                        type: 'oneCellAnchor',
                                        from: {
                                            col: 9,
                                            colOff: '2mm',
                                            row: this.row,
                                            rowOff: '2mm',
                                        },
                                        x:'10mm',
                                        y:'10mm'
                                    },
                                })
                            } catch (error) {
                                console.log(error.message, 'message')
                            }
                        }
                    }
                }
            }
            resolve()
        })
    }

    footerRow(){
        const style = this.headerStyle(true)
        style.alignment.horizontal = 'right'
        const priceFormat = this.wb.createStyle(this.priceStyle())
        this.row = this.row + 1
        this.ws.row(this.row).setHeight(30)
        this.ws.cell(this.row, 1, this.row, 6, true).style(style).string('TOTAL')
        this.ws.cell(this.row, 7).style(priceFormat).formula(`SUM(G9:G${(this.row-1)})`)
    }
}
export default ExportTrackingRepository;