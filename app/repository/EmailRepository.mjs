import SendgridService from '../helper/SendgridService.mjs'
import ejs from 'ejs'
import fs from 'fs'
import path from 'path'
const __dirname = path.resolve(path.dirname('resources'))
const recources = path.join(__dirname, 'resources/views')


class EmailRepository {

    constructor(req, res){
        this.request = req
        this.response = res
    }

    execute(){
       try {
            let message =  null
            let bodyReq = this.request.body

            if(bodyReq.type == 'example'){
                message = this.messageExample()
            } 

            if(bodyReq.type == 'otp-backend') {
                message = this.messageOtpBackend();
            }

            if(bodyReq.type == 'register-seminar') message = this.messageRegisterSeminar()

            if(bodyReq.type == 'email-after-paid') message = this.messageAfterPaid()

            if(bodyReq.type == 'email-general') message = this.messageGeneral()

            if(bodyReq.type == 'email-general-v2') {
                    if(!bodyReq.platform_name) {
                        return 'platform_name required'
                    }
                    if(!bodyReq.sender_email) {
                        return 'sender_email required'
                    }
                    if(!bodyReq.recipient_email) {
                        return 'recipient_email required'
                    }
                    if(!bodyReq.recipient_name) {
                        return 'recipient_name required'
                    }
                    if(!bodyReq.subject) {
                        return 'subject required'
                    }
                    if(!bodyReq.title) {
                        return 'title required'
                    }
                    if(!bodyReq.message) {
                        return 'message required'
                    }
                message = this.messageGeneralV2()
                return message
            }

            if(bodyReq.type == 'reset-password-backend') message = this.resetPasswordBackendCom()

            if(bodyReq.type == 'account-notif-id') message = this.messageAccountNotifId()

            if(bodyReq.type == 'refund-rapyd-com' ) message = this.messageRefundRapydNotif()

            if(bodyReq.type == 'after-paid-seminar') message = this.messageAfterPaidSeminar()


            if(!message) return false
            SendgridService.sendMail(message)
            
            return true
       } catch (error) {
           throw error
           return false
       }
    }

    messageExample(){
        const html = fs.readFileSync(recources + '/mail/example.html', {encoding:'utf-8'})
        const msg = {
            to: this.request.body.to,
            from: this.request.body.from,
            subject: this.request.body.subject,
            html: ejs.render(html, {email:this.request.body.to}),
        }
        return msg
    }

    messageOtpBackend() {
        const html      = fs.readFileSync(recources + '/mail/otp-backend.html', {encoding:'utf-8'})
        let bodyData    = this.request.body
        const msg = {
            to: bodyData.to,
            from: bodyData.from,
            subject: bodyData.subject,
            html: ejs.render(html, {
                email       : bodyData.to, 
                name        : bodyData.data.name,
                title       : bodyData.subject,
                otp         : bodyData.data.otp,
                otpGoogle   : bodyData.data.otpGoogle,
                qrCode      : bodyData.data.qrCode
            }),
        }
        return msg
    }

    messageRegisterSeminar() {
        let html            = fs.readFileSync(recources + '/mail/seminar-register.html', {encoding:'utf-8'})
        let bodyData        = this.request.body
        let {dynamicEmail}  = bodyData.data
        if(typeof dynamicEmail !== 'undefined' || dynamicEmail === true) {
            html            = fs.readFileSync(recources + '/mail/importircom/after-paid-seminar.html', {encoding: 'utf-8'})
        }

        const msg = {
            to: bodyData.to,
            from: bodyData.from,
            subject: bodyData.subject,
            html: ejs.render(html, {
                data : bodyData.data
            }),
        }
        return msg
    }

    messageAfterPaid() {
        const html      = fs.readFileSync(recources + '/mail/bill/status-customer-paid.html', 'utf-8')
        let bodyData    = this.request.body

        const msg = {
            to: bodyData.to,
            from: bodyData.from,
            subject: bodyData.subject,
            html: ejs.render(html, {
                data : bodyData.data,
                token: bodyData.token,
                subject: bodyData.subject,
                url    : process.env.IMPORTIR_URL
            }),
        }
        return msg
    }

    messageGeneral() {
        const html      = fs.readFileSync(recources + '/mail/general-email.ejs', 'utf-8')
        let bodyData    = this.request.body

        return {
            to: bodyData.to,
            from: bodyData.from,
            subject: bodyData.subject,
            html: ejs.render(html, {
                data : bodyData.data,
                url    : process.env.IMPORTIR_URL,
                baseUrl : recources
            }),
        }
    }

    async messageGeneralV2() {
        try{
            const html      = fs.readFileSync(recources + '/mail/general-email-v2.ejs', 'utf-8')
            let bodyData    = await this.request.body

            if ((bodyData.recipient_email).length != (bodyData.recipient_name).length) {
                return "recipient_email and recipient_name must be the same length"
            }

            // ex: body data
            // {
            //     "type":"email-general-v2",
            //     "platform_name" : "kelontong online",
            //     "recipient_email" : ["ekodepat@gmail.com","ekodepat@gmail.com","ekodepat@gmail.com"],
            //     "recipient_name" : ["rizal","rizal","rizal"],
            //     "subject" : "testing",
            //     "title" : "-",
            //     "message" : "<h1>Email Coba<h1> </br> <p>Lorem ipsum dolor</p>",
            //     "sender_phone" : "+6281240154599",
            //     "sender_email" : "sales@importir.id",
            //     "sender_address" : "Green Lake City, Tangerang"
            // }

            bodyData.recipient_email.map( (emailTo,indx) => {
                SendgridService.sendMail({
                    to: emailTo,
                    from: bodyData.sender_email,
                    subject: bodyData.sender_email,
                    html: ejs.render(html, {
                            platform_name : bodyData.platform_name,
                            recipient_email : emailTo,
                            recipient_name : bodyData.recipient_name[indx],
                            subject : bodyData.subject,
                            title : bodyData.title,
                            message : bodyData.message
                    }),
                })

            })

            return true
        }
        catch(error){
            console.log(error)
        }
        
    }

    resetPasswordBackendCom() {
        const html      = fs.readFileSync(recources + '/mail/importircom/forgot-password-admin.ejs', 'utf-8')
        let bodyData    = this.request.body
        
        return {
            to      : bodyData.to,
            from    : bodyData.from,
            subject : bodyData.subject,
            html    : ejs.render(html, {
                data    : bodyData.data,
                url     : process.env.IMPORTIR_URL,
                baseUrl : recources 
            })
        }
    }

    messageAccountNotifId(){
        const {to} = this.request.body
        let {countryCode} = this.request.body
        let html;

        if(typeof to == 'undefined' || to == '') return 'Invalid Email'
        if(typeof countryCode == 'undefined' || to == '') { countryCode = 'id' }
        
        if (countryCode == 'id') {
            html = fs.readFileSync(recources + '/mail/importirid/notification-account.html', {encoding:'utf-8'})
        }
        else {
            html = fs.readFileSync(recources + '/mail/importirid/notification-account-ph.html', {encoding:'utf-8'})
        }
        const msg = {
            to: this.request.body.to,
            from: this.request.body.from,
            subject: this.request.body.subject,
            html: ejs.render(html, {email:to}),
        }
        return msg
    }

    messageRefundRapydNotif(){
        const html      = fs.readFileSync(recources + '/mail/importircom/refund-rapyd.ejs', {encoding:'utf-8'})
        
        return {
            to      : this.request.body.to,
            from    : this.request.body.from,
            subject : this.request.body.subject,
            html    : ejs.render(html, {data : this.request.body.data, url: process.env.IMPORTIR_URL, baseUrl : recources })
        }
    }

    messageAfterPaidSeminar(){
        const html      = fs.readFileSync(recources + '/mail/importircom/after-paid-seminar.html', {encoding: 'utf-8'})

        return {
            to      : this.request.body.to,
            from    : this.request.body.from,
            subject : this.request.body.subject,
            html    : ejs.render(html, {data : this.request.body.data})
        }
    }
}

export default EmailRepository