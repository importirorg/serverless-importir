import QueryRepository from './QueryRepository.mjs'
import fetch from 'node-fetch'
import excel from 'excel4node'
import {resizeImageByParamsUrl, getTokenAzure, capitalize} from '../helper/index.mjs'

import AwsService from '../helper/AwsService.mjs'

class GeneralExportRepository extends QueryRepository{

    constructor(boxId, type = 'sea', image = false){
        super()
        this.image = image
        this.boxID = boxId
        this.wb = new excel.Workbook()
        this.ws = this.wb.addWorksheet('Sheet 1')
        this.row = 3
        this.type = type
        this.getToken()
        this.productNeedTranslate = []
    }

    async getToken(){
        try {
            const token = await getTokenAzure()
            this.token = token
        } catch (error) {
            throw error
        }
    }

    async execute(){
        try {
            let pl = await this.findYgBoxById(this.boxID)
            let order = pl.orders.filter(x => x.type != 'SHP' && x.yg_order !== null)
            let shipping = pl.orders.filter(x => x.type === 'SHP' && x.shipping !== null)
            // find admin prioduct and translate product
            await this.generateOrderTitle(order)
            this.generateHeaderDocument()
            this.generateHeaderTable()
            await this.generateShippingBody(shipping)
            await this.generateOrderBody(order)
            this.generateFooterDocument()
            console.log('finished')
            const fileName = `packing-list-${this.boxID}.xlsx`
            const fileCheck = await AwsService.checkFile(fileName)

            // delete file if exist
            if(fileCheck){
                await AwsService.deleteFile(fileName)
            }
            const stream = await this.wb.writeToBuffer()
            const upload = await AwsService.uploadExcel(stream, fileName)
            return upload
            // return shipping
        } catch (error) {
            return error
        }
    }

    generateOrderTitle(order){
       return new Promise(async(resolve, reject) => {
           let product = order.filter(x => {
               return x.yg_order.yg_order_detail[0].yg_product_detail === null
           })
            if(product.length){
                let dump = product.map(x => {
                    let c = JSON.parse(x.yg_order.official_dump)
                    let name = ''
                    if(c){
                        name = c.productItems[0].name
                    }
                    return {
                        order_id:x.yg_order.id,
                        dump:c,
                        title:name
                    }
                })
                this.productNeedTranslate = dump
                let tr = await this.generateTitleString()
                resolve(tr)
           }
           resolve(null)
       })
    }
    
    generateTitleString(){
        if(!this.productNeedTranslate) return false
        return new Promise((resolve, reject)=>{
            let str = this.productNeedTranslate.map(x => {
                return `${x.order_id} DED ${x.title} DEDY,`
            })
            str = str.toString()
            this.translate('en', str).then(res => {
                let result = res.split('DEDY,')
                result = result.filter(x => x !== '')
                result.map(x => {
                    let arr = x.split('DED')
                    let indexOrder = this.productNeedTranslate.findIndex(y => y.order_id == arr[0])
                    this.productNeedTranslate[indexOrder].title_en = arr[1]
                })
                resolve(result)
            })
            .catch(err => reject(err))
        })
    }

    cellOrderBody(params, bodyStyle){
        return new Promise(async(resolve, rejecr) => {
            this.ws.row(this.row).setHeight(90)
            this.ws.cell(this.row, 1).style(bodyStyle).string('')
            this.ws.cell(this.row, 2).style(bodyStyle).string('')
            this.ws.cell(this.row, 3).style(bodyStyle).string(`YG${params.yg_order}`)
            this.ws.cell(this.row, 4).style(bodyStyle).string(params.product_title)
            this.ws.cell(this.row, 5).style(bodyStyle).string(params.product_title_en)
            this.ws.cell(this.row, 6).style(bodyStyle).number(params.carton_total)
            this.ws.cell(this.row, 7).formula(`H${this.row}/F${this.row}`).style(bodyStyle)
            this.ws.cell(this.row, 8).style(bodyStyle).number(params.quantity)
            this.ws.cell(this.row, 9).style(bodyStyle).number(params.dimension_height)
            this.ws.cell(this.row, 10).style(bodyStyle).number(params.dimension_width)
            this.ws.cell(this.row, 11).style(bodyStyle).number(params.dimension_length)
            this.ws.cell(this.row, 12).style(bodyStyle).number(params.cbm_per_carton)
            this.ws.cell(this.row, 13).style(bodyStyle).number(params.cbm_total)

            // this.ws.cell(this.row, 14).style(bodyStyle).formula(`M${this.row}`)
            // this.ws.cell(this.row, 15).style(bodyStyle).formula(`P${this.row}/F${this.row}`)

            this.ws.cell(this.row, 14).style(bodyStyle).number(params.weight)
            this.ws.cell(this.row, 15).style(bodyStyle).formula(`F${this.row}*N${this.row}`)
            this.ws.cell(this.row, 16).style(bodyStyle).number(params.price_per_product)
            this.ws.cell(this.row, 17).style(bodyStyle).formula(`H${this.row}*P${this.row}`)
            this.ws.cell(this.row, 18).style(bodyStyle).string('No Brand')
            this.ws.cell(this.row, 19).style(bodyStyle).string('Paper Carton')

            if(this.type == 'air'){
                this.ws.cell(this.row, 20).string(params.user_name).style(bodyStyle)
                this.ws.cell(this.row, 21).string(params.user_phone).style(bodyStyle)
                this.ws.cell(this.row, 22).string(capitalize(params.user_province)).style(bodyStyle)
                this.ws.cell(this.row, 23).string(capitalize(params.user_city)).style(bodyStyle)
                this.ws.cell(this.row, 24).string(capitalize(params.user_address)).style(bodyStyle)
                this.ws.cell(this.row, 25).string(params.contains).style(bodyStyle)
                this.ws.cell(this.row, 26).style(bodyStyle).string('')
                if(this.image){
                    let uriImg = params.image
                    let img = false
                    if(params.flag == 'admin_product' || params.flag == 'taobao' || params.flag == 'tmall'){
                        img = await resizeImageByParamsUrl(uriImg, true)
                    }else{
                        img = await resizeImageByParamsUrl(uriImg, false)
                    }
                    if(img){
                        await this.ws.addImage({
                            image: img,
                            type: 'picture',
                            width:'15mm',
                            position: {
                                type: 'oneCellAnchor',
                                from: {
                                    col: 28,
                                    colOff: '2mm',
                                    row: this.row,
                                    rowOff: '2mm',
                                },
                                x:'10mm',
                                y:'10mm'
                            },
                        })
                    }
                }
            }else{
                this.ws.cell(this.row, 20).style(bodyStyle).string('')
                this.ws.cell(this.row, 21).style(bodyStyle)
                if(this.image){
                    let uriImg = params.image
                    let img = false
                    if(params.flag == 'admin_product' || params.flag == 'taobao' || params.flag == 'tmall'){
                        img = await resizeImageByParamsUrl(uriImg, true)
                    }else{
                        img = await resizeImageByParamsUrl(uriImg, false)
                    }
                    if(img){
                        try {
                            await this.ws.addImage({
                                image: img,
                                type: 'picture',
                                width:'15mm',
                                position: {
                                    type: 'oneCellAnchor',
                                    from: {
                                        col: 23,
                                        colOff: '2mm',
                                        row: this.row,
                                        rowOff: '2mm',
                                    },
                                    x:'10mm',
                                    y:'10mm'
                                },
                            })
                        } catch (error) {
                            console.log(error.message, 'message')
                        }
                    }
                }
            }

            resolve('row finished')
        })
    }

    generateOrderBody(order){
        return new Promise(async(resolve, reject) => {
            const bodyStyle = this.wb.createStyle(this.fillStyle())
            for (let index = 0; index < order.length; index++) {
                let ygOrder = order[index].yg_order
                let detail = ygOrder.yg_order_detail[0]
                let product = detail.yg_product_detail
                if(!product){
                    let check = this.productNeedTranslate.findIndex(x => x.order_id == ygOrder.id)
                    if(check >= 0){
                        product = this.productNeedTranslate[check]
                    }else{
                        product = {
                            title:'',
                            title_en:''
                        }
                    }
                }

                const cbmTotal = parseFloat(ygOrder.cbm_total)
                let cbmPerCarton = (cbmTotal / parseFloat(ygOrder.carton_total))
                cbmPerCarton = isNaN(cbmPerCarton) ? 0 : cbmPerCarton

                let totalProductRmb = parseFloat(detail.price_actual_all_product) / parseFloat(detail.rates_rmb)
                let pricePerProduct = totalProductRmb / parseFloat(detail.quantity)
                pricePerProduct = pricePerProduct ? pricePerProduct : 0

                let params = {
                    yg_order:ygOrder.id,
                    product_title:product.title,
                    product_title_en:product.title_en,
                    carton_total:ygOrder.carton_total,
                    quantity:detail.quantity,
                    dimension_height:ygOrder.dimension_height,
                    dimension_width:ygOrder.dimension_width,
                    dimension_length:ygOrder.dimension_length,
                    cbm_per_carton:cbmPerCarton,
                    cbm_total:cbmTotal,
                    weight:ygOrder.weight,
                    price_per_product:pricePerProduct,
                    user_name:ygOrder.user.name,
                    user_phone:ygOrder.user.phone,
                    user_province:detail.province,
                    user_city:detail.city,
                    user_address:detail.address,
                    contains:detail.contains,
                    image:product.image,
                    flag:product.flag
                }

                let packingReceipt = ygOrder.yg_packing_receipts
                let lastCtns = 0;

                if(packingReceipt.length > 0){
                    for(let idx = 0; idx < packingReceipt.length; idx++){
                        let receipts = packingReceipt[idx]
                        params.carton_total = receipts.ctn_qty
                        params.dimension_height = receipts.height
                        params.dimension_width = receipts.width
                        params.dimension_length = receipts['length']
                        params.cbm_per_carton = receipts.cbm
                        params.cbm_total = receipts.cbm * parseInt(receipts.ctn_qty)
                        params.quantity = receipts.qty * parseInt(receipts.ctn_qty)
                        params.weight = receipts.weight
                        
                        lastCtns += parseInt(receipts.ctn_qty)
                        var minCtn = lastCtns - parseInt(receipts.ctn_qty) + 1

                        if(minCtn == lastCtns){
                            params.ctns = minCtn.toString()
                        }else{
                            params.ctns = `${minCtn}-${lastCtns}`
                        }

                        this.row++
                        await this.cellOrderBody(params, bodyStyle)
                    }
                }else{
                    this.row++
                    params.ctns = '1'
                    await this.cellOrderBody(params, bodyStyle)
                }
            }
            resolve('finished')
        })
    }

    translate(to, text){
        return new Promise((resolve, reject) => {
            if(!text) resolve('')
            const params = [{text:text}]
            fetch(`https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&to=${to}&plain=plain`, {
                headers:{
                    'Authorization': 'Bearer ' + this.token,
                    'Content-type': 'application/json',
                    'cache-control': 'no-cache'
                },
                body:JSON.stringify(params),
                method:'POST',
                json: true,
            })
            .then(res => res.json())
            .then(res => {
                if(typeof res.error != 'undefined'){
                    resolve(res[0].translations[0].text)
                }else{
                    resolve('')
                }
            })
            .catch(err => reject(err))
        })
    }

    generateHeaderDocument(){
        let cell = 21
        if(this.type == 'air'){
            cell = 26
        }
        this.ws.cell(1, 1, 1, cell, true).string('PACKING LIST').style({
            font: {name: 'Times New Roman', bold:true, size:20}, 
            alignment:{
                horizontal: 'center',
                vertical: 'center'
            }
        });
        this.ws.row(1).setHeight(35)
    }

    generateHeaderTable(){
        const headerStyle = (bold = true) => this.wb.createStyle({
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            font: {
                name: 'Times New Roman',
                bold: bold,
                size: bold ? 11 : 9
            },
            border: {
                left: {
                    style: 'thin',
                    color: 'black' 
                },
                right: {
                    style: 'thin',
                    color: 'black'
                },
                top: {
                    style: 'thin',
                    color: 'black'
                },
                bottom: {
                    style: 'thin',
                    color: 'black'
                }
            }
        });
        this.ws.row(2).setHeight(30)
        this.ws.row(4).setHeight(30)
        this.ws.cell(2, 1).string('Dates').style(headerStyle(true))
        this.ws.column(1).setWidth(10);
        this.ws.cell(3, 1).string('日期').style(headerStyle(false))

        this.ws.cell(2, 2).string('Ctns No').style(headerStyle(true))
        this.ws.column(2).setWidth(10);
        this.ws.cell(3, 2).string('箱号').style(headerStyle(false))

        this.ws.cell(2, 3).string('Marking').style(headerStyle(true))
        this.ws.column(3).setWidth(15);
        this.ws.cell(3, 3).string('唛头').style(headerStyle(false))

        this.ws.cell(2, 4, 3, 5, true).string('品名 / DESC GOODS').style(headerStyle(true))
        this.ws.column(4).setWidth(15);
        this.ws.column(5).setWidth(15);
        this.ws.cell(3, 4).string('中文品名').style(headerStyle(false))
        this.ws.cell(3, 5).string('英文品名').style(headerStyle(false))

        this.ws.cell(2, 6).string('Packages').style(headerStyle(true))
        this.ws.cell(3, 6).string('件数').style(headerStyle(false))

        this.ws.column(6).setWidth(10);
        this.ws.cell(2, 7).string('Pcs/ctn').style(headerStyle(true))
        this.ws.cell(3, 7).string('每箱数量').style(headerStyle(false))

        this.ws.column(7).setWidth(15);
        this.ws.cell(2, 8).string('Total Pcs').style(headerStyle(true))
        this.ws.column(8).setWidth(15);
        this.ws.cell(3, 8).string('数量').style(headerStyle(false))

        this.ws.cell(2, 9, 2, 11, true).string('Size cm（厘米) / Ctn').style(headerStyle(true))
        this.ws.column(9).setWidth(6);
        this.ws.column(10).setWidth(6);
        this.ws.column(11).setWidth(6);
        this.ws.cell(3, 9).string('L (长)').style(headerStyle(false))
        this.ws.cell(3, 10).string('W (宽)').style(headerStyle(false))
        this.ws.cell(3, 11).string('H (高)').style(headerStyle(false))

        this.ws.cell(2, 12, 2, 13, true).string('CBM/立方').style(headerStyle(true))
        this.ws.column(12).setWidth(10);
        this.ws.column(13).setWidth(10);
        // this.ws.column(14).setWidth(10);
        this.ws.cell(3, 12).string('立方/箱').style(headerStyle(false))
        this.ws.cell(3, 13).string('总立方').style(headerStyle(false))
        // this.ws.cell(3, 14).string('TOTAL/CBM').style(headerStyle(false))

        this.ws.cell(2, 14, 2, 15, true).string('WEIGHT/重量 KGS').style(headerStyle(true))
        this.ws.column(14).setWidth(10);
        this.ws.column(15).setWidth(10);
        // this.ws.column(16).setWidth(10);
        this.ws.cell(3, 14).string('Weight/Ctn (重量/箱)').style(headerStyle(false))
        this.ws.cell(3, 15).string('Total Weight (重量/箱)').style(headerStyle(false))
        // this.ws.cell(3, 17).string('重量/箱').style(headerStyle(false))

        this.ws.cell(2, 16, 2, 17, true).string('VALUE/价格（RMB)').style(headerStyle(true))
        this.ws.column(16).setWidth(10);
        this.ws.column(17).setWidth(10);
        this.ws.cell(3, 16).string('Unit Price (单价)').style(headerStyle(false))
        this.ws.cell(3, 17).string('Total Price (总金额)').style(headerStyle(false))

        this.ws.cell(2, 18, 3, 18, true).string('Brand').style(headerStyle(true))
        this.ws.cell(2, 19, 3, 19, true).string('Packing / 包装类型').style(headerStyle(true))
        this.ws.column(19).setWidth(15);
        if(this.type == 'air'){
            this.ws.cell(2, 20, 3, 20, true).string('Name').style(headerStyle(true))
            this.ws.column(20).setWidth(15);
            this.ws.cell(2, 21, 3, 21, true).string('Phone').style(headerStyle(true))
            this.ws.column(21).setWidth(15);
            this.ws.cell(2, 22, 3, 22, true).string('Province').style(headerStyle(true))
            this.ws.column(22).setWidth(15);
            this.ws.cell(2, 23, 3, 23, true).string('City').style(headerStyle(true))
            this.ws.column(23).setWidth(15);
            this.ws.cell(2, 24, 3, 24, true).string('Address').style(headerStyle(true))
            this.ws.column(24).setWidth(20);
            this.ws.cell(2, 25, 3, 25, true).string('Note').style(headerStyle(true))
            this.ws.column(25).setWidth(20);
            this.ws.cell(2, 26, 3, 26, true).string('Image').style(headerStyle(true))
            this.ws.column(26).setWidth(16)
        }else{
            this.ws.cell(2, 20, 3, 20, true).string('Note').style(headerStyle(true))
            this.ws.column(20).setWidth(20);
            this.ws.cell(2, 21, 3, 21, true).string('Image').style(headerStyle(true))
            this.ws.column(21).setWidth(16)
        }
    }

    generateFooterDocument(){
        const headerStyle = (bold = true) => this.wb.createStyle({
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            font: {
                name: 'Times New Roman',
                bold: bold,
                size: bold ? 11 : 9
            },
            border: {
                left: {
                    style: 'thin',
                    color: 'black' 
                },
                right: {
                    style: 'thin',
                    color: 'black'
                },
                top: {
                    style: 'thin',
                    color: 'black'
                },
                bottom: {
                    style: 'thin',
                    color: 'black'
                }
            }
        });
        this.row = this.row + 1
        this.ws.row(this.row).setHeight(30)
        this.ws.cell(this.row, 1, this.row, 5, true).style(headerStyle()).string('TOTAL')
        this.ws.cell(this.row, 6).formula(`SUM(F4:F${(this.row-1)})`).style(headerStyle())
        this.ws.cell(this.row, 7, this.row, 12, true).string('').style(headerStyle())
        this.ws.cell(this.row, 13).formula(`SUM(M4:M${(this.row-1)})`).style(headerStyle())

        this.ws.cell(this.row, 15).formula(`SUM(O4:O${(this.row-1)})`).style(headerStyle())
        // this.ws.cell(this.row, 16).formula(`SUM(04:0${(this.row-1)})`).style(headerStyle())
        this.ws.cell(this.row, 17).formula(`SUM(Q4:Q${(this.row-1)})`).style(headerStyle())
        this.ws.cell(this.row, 18).string('').style(headerStyle())

        let cell = 21
        if(this.type == 'air'){
            cell = 26
        }
        console.log(cell, 'cel')
        this.ws.cell(this.row, 19, this.row, cell, true).string('').style(headerStyle())
    }

    generateShippingProduct(product, packCartonQty, packCarton){
        if(product.length > 1){
            let title = ''
            let priceRMB = []
            let calcPrice = []
            let pricePerProduct = 0
            let status = false
            let total = 0
            let img = ''
            for (let index = 0; index < product.length; index++) {
                let pr = product[index]
                title += pr.products.name+','
                priceRMB.push(pr.products.unit_price_rmb)
                let c = pr.products.unit_price_rmb * pr.qty *  packCarton
                calcPrice.push(c)
                img = pr.products.image
            }

            let min = Math.min.apply(null, priceRMB)
            let max = Math.max.apply(null, priceRMB)
            if (min == max) {
                pricePerProduct = priceRMB[0]
                status = true
            } else {
                total = calcPrice.reduce((a, b) => a + b , 0)
                pricePerProduct = total / packCartonQty
                status = false
            }
            return {
                title:title.slice(0, -1),
                price:pricePerProduct,
                status:status,
                total:total,
                image:img
            }
        }else if(product.length == 1){
            if(product[0].products == null){
                return {
                    title:'',
                    price:0,
                    total:0,
                    status:true,
                    image:''
                }
            };
            return {
                title:product[0].products.name,
                price:parseFloat(product[0].products.total_price_rmb) / parseFloat(product[0].products.quantity),
                total:product[0].products.total_price_rmb / product[0].products.quantity,
                status:true,
                image:product[0].products.image
            }
        }else{
            return {
                title:'',
                price:0,
                total:0,
                status:true,
                image:''
            }
        }


    }

    generateShippingBody(shipping){
        return new Promise(async(resolve, reject) => {
            const bodyStyle = this.wb.createStyle(this.fillStyle())
            
            for(let i=0; i<shipping.length; i++){
                let shp = shipping[i].shipping
                let pack    = shp.packs
                if(shp){
                    for(var k = 0; k < pack.length; k++){
                        let packShp = pack[k]
                        // console.log(packShp, k, 'sksk')
                        let product = this.generateShippingProduct(packShp.products, packShp.carton_qty, packShp.carton)
                        this.row++
                        this.ws.row(this.row).setHeight(90)
                        this.ws.cell(this.row, 1).style(bodyStyle).string('')
                        this.ws.cell(this.row, 2).style(bodyStyle).string('')
                        this.ws.cell(this.row, 3).style(bodyStyle).string(`SHP${packShp.shipping_id}`)
                        this.ws.cell(this.row, 4).style(bodyStyle).string(product.title)
                        this.ws.cell(this.row, 5).style(bodyStyle).string(product.title)
                        this.ws.cell(this.row, 6).style(bodyStyle).number(packShp.carton)
                        this.ws.cell(this.row, 7).formula(`H${this.row}/F${this.row}`).style(bodyStyle)
                        this.ws.cell(this.row, 8).style(bodyStyle).number(packShp.carton_qty)
                        this.ws.cell(this.row, 9).style(bodyStyle).number(packShp.height)
                        this.ws.cell(this.row, 10).style(bodyStyle).number(packShp.width)
                        this.ws.cell(this.row, 11).style(bodyStyle).number(packShp.length)
                        this.ws.cell(this.row, 12).style(bodyStyle).formula(`I${this.row}*J${this.row}*K${this.row}/1000000`)
                        this.ws.cell(this.row, 13).style(bodyStyle).formula(`L${this.row}*F${this.row}`)
                        // this.ws.cell(this.row, 14).style(bodyStyle).formula(`M${this.row}`)
                        this.ws.cell(this.row, 14).style(bodyStyle).number(packShp.weight)
                        let totalWeight = parseFloat(packShp.carton) * parseFloat(packShp.weight)
                        this.ws.cell(this.row, 15).style(bodyStyle).number(totalWeight)
                        // this.ws.cell(this.row, 17).style(bodyStyle).number(totalWeight)
                        if(product.status){
                            this.ws.cell(this.row, 16).style(bodyStyle).number(product.price)
                            this.ws.cell(this.row, 17).style(bodyStyle).formula(`P${this.row}*H${this.row}`)
                        }else{
                            this.ws.cell(this.row, 16).style(bodyStyle).number(product.price)
                            this.ws.cell(this.row, 17).style(bodyStyle).number(product.total)
                        }
                        this.ws.cell(this.row, 18).style(bodyStyle).string('No Brand')
                        this.ws.cell(this.row, 19).style(bodyStyle).string('paper carton')
                        
                        if(this.type == 'air'){
                            this.ws.cell(this.row, 20).string(shp.user.name).style(bodyStyle)
                            this.ws.cell(this.row, 21).string(shp.user.phone).style(bodyStyle)
                            this.ws.cell(this.row, 22).string(capitalize(shp.user.city)).style(bodyStyle)
                            this.ws.cell(this.row, 23).string(capitalize(shp.user.city)).style(bodyStyle)
                            this.ws.cell(this.row, 24).string(capitalize(shp.user.full_address)).style(bodyStyle)
                            this.ws.cell(this.row, 25).style(bodyStyle).string('').style(bodyStyle)
                            this.ws.cell(this.row, 26).style(bodyStyle).string('').style(bodyStyle)
                            if(this.image){
                                let uriImg = product.image
                                let img = await resizeImageByParamsUrl(process.env.CDN_URL+uriImg, true)
                                if(img){
                                    await this.ws.addImage({
                                        image: img,
                                        type: 'picture',
                                        width:'15mm',
                                        position: {
                                            type: 'oneCellAnchor',
                                            from: {
                                                col: 28,
                                                colOff: '2mm',
                                                row: this.row,
                                                rowOff: '2mm',
                                            },
                                            x:'10mm',
                                            y:'10mm'
                                        },
                                    })
                                }
                            }
                        }else{
                            this.ws.cell(this.row, 20).style(bodyStyle).string('')
                            this.ws.cell(this.row, 21).style(bodyStyle)
                            if(this.image){
                                let uriImg = product.image
                                let img = await resizeImageByParamsUrl(process.env.CDN_URL+uriImg, true)
                                if(img){
                                    await this.ws.addImage({
                                        image: img,
                                        type: 'picture',
                                        width:'15mm',
                                        position: {
                                            type: 'oneCellAnchor',
                                            from: {
                                                col: 23,
                                                colOff: '2mm',
                                                row: this.row,
                                                rowOff: '2mm',
                                            },
                                            x:'10mm',
                                            y:'10mm'
                                        },
                                    })
                                }
                            }
                        }
                    }
                }
            }

            resolve('finished')
        })
    }

    fillStyle(){
        return {
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            font: {
                name: 'Times New Roman',
                size: 12
            },
            border: {
                left: {
                    style: 'thin', //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                    color: "black" // HTML style hex value
                },
                right: {
                    style: 'thin',
                    color: "black"
                },
                top: {
                    style: 'thin',
                    color: "black"
                },
                bottom: {
                    style: 'thin',
                    color: "black"
                }
            }
        }
    }
}

export default GeneralExportRepository;