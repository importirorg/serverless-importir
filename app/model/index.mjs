import database from '../config/database.mjs'

// set Model shipping container
const ShippingContainers = database.model('ShippingContainers', {
    hasTimestamps: true,
    tableName : 'shipping_containers',
    idAttribute : 'id',

    shippings(){
        return this.hasMany(Shipping, 'container_id')
    },

    yg_order(){
        return this.hasMany(YgOrder, 'container_id')
    }

})

const YgProductDetail = database.model('YgProductDetail', {
    tableName: 'yg_product_detail',
    softDelete:true
})

const YgOrderDetail = database.model('YgOrderDetail', {
    tableName: 'yg_order_detail',
    softDelete:true,
    yg_product_detail: function() {
        return this.belongsTo(YgProductDetail, 'yg_product_id', 'product_id')
    }
})

const YgOrder = database.model('YgOrder', {
    tableName: 'yg_order',
    softDelete:true,
    yg_order_detail: function() {
        return this.hasMany(YgOrderDetail, 'yg_order_id')
    },
    user: function() {
        return this.hasOne(OrgUsers, 'user_id', 'id')
    },
    yg_packing_receipts(){
        return this.hasMany(YgPackingReceipts, 'yg_order_id')
    }
})

const ShippingProducts = database.model('ShippingProducts', {
    tableName: 'shipping_products',
    softDelete:true
})

const PackingProducts = database.model('PackingProducts', {
    tableName: 'packing_products',
    softDelete:true,
    products: function() {
        return this.hasOne(ShippingProducts, 'product_id')
    }
})

const YgPackingReceipts = database.model('YgPackingReceipts', {
    tableName: 'yg_packing_receipt',
    softDelete:true
})

const PackingReceipts = database.model('PackingReceipts', {
    tableName: 'packing_receipts',
    softDelete:true,
    products: function() {
        return this.hasMany(PackingProducts, 'packing_id')
    }
})

const OrgUsers  = database.model('OrgUsers', {
    tableName: 'org_users',
    softDelete:true
})

const Shipping  = database.model('Shipping', {
    tableName: 'shipping',
    softDelete:true,
    user:function() {
        return this.hasOne(OrgUsers, 'user_id', 'id')
    },
    packs: function() {
        return this.hasMany(PackingReceipts, 'shipping_id')
    },
    shipping_product: function() {
        return this.hasMany(ShippingProducts, 'shipping_id')
    }
})

const YgBoxOrder = database.model('YgBoxOrder', {
    tableName: 'yg_box_order',
    softDelete:true,
    shipping:function() {
        return this.hasOne(Shipping, 'shipping_id', 'id')
    },
    yg_order: function() {
        return this.hasOne(YgOrder, 'yg_order_id', 'id')
    }
})

const YgBox = database.model('YgBox', {
    tableName: 'yg_box',
    softDelete:true,
    hasTimestamps: true,
    orders:function() {
        return this.hasMany(YgBoxOrder, 'yg_box_id').orderBy('type')
    }
})

export {
    ShippingContainers,
    YgProductDetail,
    YgOrderDetail,
    YgOrder,
    ShippingProducts,
    PackingProducts,
    PackingReceipts,
    OrgUsers,
    Shipping,
    YgBoxOrder,
    YgBox
}