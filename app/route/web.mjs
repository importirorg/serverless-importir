import express from 'express'
import path from 'path'
import ejs from 'ejs'
import WelcomeController from '../controller/WelcomeController.mjs'
import ExportController from '../controller/ExportController.mjs'
import TranslateController from '../controller/TranslateController.mjs'
import EmailServiceController from '../controller/EmailServiceController.mjs'

const web = express()
const __dirname = path.resolve(path.dirname(''))

web.set('view engine','ejs')
web.set('views', path.join(__dirname, 'resources/views'))
web.engine('html', ejs.renderFile)



web.get('/welcome', WelcomeController.index)

// export route
web.get('/shipping-tracking-export/:id', ExportController.shippingTracking)
web.get('/general-export/:id', ExportController.generalExport)
web.get('/translate', TranslateController.index)

// mail router
web.get('/mail-service', EmailServiceController.index)
web.post('/mail-service', EmailServiceController.postMail)

export default web
