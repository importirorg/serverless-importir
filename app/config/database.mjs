import dotenv from 'dotenv'
dotenv.config();
import knex from 'knex'
import bookshelf from 'bookshelf'
import eloquent from 'bookshelf-eloquent'
import softDeletes   from 'bookshelf-paranoia'



const main = knex({
    client: 'mysql',
    connection: {
      host     : process.env.DB_HOST,
      user     : process.env.DB_USER,
      password : process.env.DB_PASSWORD,
      database : process.env.DB_NAME,
      charset  : 'utf8'
    }
});

const DB = bookshelf(main)
DB.plugin('registry')
DB.plugin(eloquent)
DB.plugin(softDeletes, { field: 'deleted_at' });

// export default DB

export default DB;