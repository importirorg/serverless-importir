import jimp from 'jimp'
import fetch from 'node-fetch'

async function resizeImage(img, w, h){
    try {
        let res = await jimp.read(img)
        let resize = res.resize(w, h)
        let mimeImg = res._originalMime
        let mime = jimp.MIME_JPEG
        if (mimeImg == 'image/png') {
            mime = jimp.MIME_PNG
        }
        let buffer = await resize.getBufferAsync(mime)
        let imgBufStr   = buffer.toString('base64')
        imgBufStr   = Buffer.from(imgBufStr, 'base64')
        return imgBufStr
    } catch (error) {
        throw error
    }
}


async function resizeImageByParamsUrl(params, type = false){
    if(!params) return false
    try {
        let result = params
        if(type){
            // result = params
            result = `https://aiqdpnthsq.cloudimg.io/v7/${params}?func=crop&width=100&height=100`
        }else{
            result = params.replace('jpg', '100x100.jpg')
        }
        console.log(result, 'result')
        let check = await jimp.read(result)
        let mime = jimp.MIME_JPEG
        let buffer = await check.getBufferAsync(mime)
        let imgBufStr   = buffer.toString('base64')
        imgBufStr   = Buffer.from(imgBufStr, 'base64')
        return imgBufStr
    } catch (error) {
        console.log(error)
        return false
    }
}

function getTokenAzure(){
    return new Promise((resolve, reject) => {
        fetch('https://southeastasia.api.cognitive.microsoft.com/sts/v1.0/issueToken?Subscription-Key=82ccc3f997324e539879988c531f5d66', {
            method:'POST',
            headers:{
                'Content-Length': 0,
                "Host": "southeastasia.api.cognitive.microsoft.com"
            }
        })
        .then(res => res.text())
        .then(res => {
            return resolve(res)
        })
        .catch(err => reject(err))
    })
}

function capitalize(strings){
    if(!strings) return ''
    return strings.charAt(0).toUpperCase() + strings.slice(1)
}

export { resizeImage, resizeImageByParamsUrl, getTokenAzure, capitalize }