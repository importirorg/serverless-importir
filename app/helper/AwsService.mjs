import AWS from 'aws-sdk'

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_S3_ACCESS_KEY,
    secretAccessKey: process.env.AWS_S3_SECRET_KEY,
    region:process.env.AWS_S3_REGION_KEY
});

class AwsService {

    constructor(){

        this.service = s3
    }

    uploadExcel(file, filename){
        return new Promise((resolve, reject) => {
            const params = {
                Bucket: process.env.AWS_S3_BUCKET_NAME,
                Key: 'shipping-tracking/' + filename, // File name you want to save as in S3
                Body: file
            }

            this.service.upload(params, function(err, data) {
                if (err) {
                    reject(err)
                }
                console.log(`File uploaded successfully. ${data.Location}`);
                resolve(data)
            });
        })
    }

    checkFile(filename){
        return new Promise((resolve, reject) => {
            const params = {
                Bucket: process.env.AWS_S3_BUCKET_NAME,
                Key: 'shipping-tracking/' + filename,
            }
            this.service.headObject(params, (err, resp, body) => {
                if(err){
                    resolve(false)
                }else{
                    resolve(true)
                }
            })
        })
    }

    deleteFile(filename){
        return new Promise((resolve, reject) => {
            const params = {
                Bucket: process.env.AWS_S3_BUCKET_NAME,
                Key: 'shipping-tracking/' + filename,
            }
            this.service.deleteObject(params, (err, data) => {
                if(err){
                    console.log(err.message)
                    resolve(false)
                }else{
                    resolve(true) // file deleted
                }
            })
        })
    }

}

export default new AwsService()