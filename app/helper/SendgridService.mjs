// const sgMail = require('@sendgrid/mail');
import sgMail from '@sendgrid/mail'
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const SendgridService = {

    async sendMail(msg){
        try {
            const execute = await sgMail.send(msg)
            return true
        } catch (error) {
            throw false
        }
    }
}

export default SendgridService