import ExportTrackingRepository from '../repository/ExportTrackingRepository.mjs'
import GeneralExportRepository from '../repository/GeneralExportRepository.mjs'

const ExportController = {

    async shippingTracking(req, res){
        try {
            const shpID = req.params.id
            const includeImage = req.query.image ? true : false
            const repo = new ExportTrackingRepository(includeImage)
            
            let shp = await repo.generateShippingTracking(shpID)
            if(!shp) return res.send('Error to export file')
            res.redirect(shp.Location);
        } catch (error) {
            console.log(error)
            res.send(error)
        }
    },

    async generalExport(req, res){
        try {
            const id = req.params.id
            const type = req.query.type ? req.query.type : false
            const image = req.query.image ? req.query.image : false
            const repo = new GeneralExportRepository(id, type, image)
            const pl = await repo.execute()
            res.redirect(pl.Location)
        } catch (error) {
            res.send(error.message)
        }
    }


}

export default ExportController