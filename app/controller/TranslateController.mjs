import translate from '@iamtraction/google-translate'

const TranslateController = {

    async index(req, res){
       try {
        const strings = req.query.text
        const result = await translate(`${strings}`, 'en');
        res.json({
            status:true,
            data: {
				origin: result.from.language.iso,
				result: result.text,
			},
        })
       } catch (error) {
           res.json({
               status:false,
               data:error.message
           })
       }
    }

}

export default TranslateController