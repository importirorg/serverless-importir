import EmailRepostory from '../repository/EmailRepository.mjs'

const EmailServiceController = {

    async index(req, res){
        res.render('mail/example.html', {email:'dedy@croot.com'});  
    },

    async postMail(req, res){
        const mailRepo = new EmailRepostory(req, res)
        const send = mailRepo.execute()
        res.send('success')
    }
}

export default EmailServiceController
