import express from 'express'
import dotenv from 'dotenv'
dotenv.config();

// import router
import router from './app/route/web.mjs'

const app = express()
const PORT = process.env.APP_PORT
const HOST = process.env.APP_HOST

app.use(express.json())

app.use('/', router)

app.listen(PORT, ()=>{
    console.log(`App listening at http://${HOST}:${PORT}`)
})